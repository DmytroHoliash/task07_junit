package com.holiash;

import java.util.Scanner;

public class Application {

  private static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    Menu menu = new Menu();
    int choice;
    while (true) {
      menu.print();
      choice = scanner.nextInt();
      if (choice == 9) {
        break;
      }
      menu.execute(choice);
    }
  }
}
