package com.holiash.longestplateau.controller;

import com.holiash.longestplateau.model.LongestPlateau;
import com.holiash.longestplateau.view.PlateauView;

public class LongestPlateauController {

  private LongestPlateau model;
  private PlateauView view;

  public LongestPlateauController(int[] array, PlateauView view) {
    this.model = new LongestPlateau(array);
    this.view = view;
  }

  public LongestPlateauController(LongestPlateau plateau, PlateauView view) {
    this.model = plateau;
    this.view = view;
  }

  public void execute() {
    this.view.show(this.model.findLongestPlateau());
  }
}
