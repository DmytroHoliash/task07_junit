package com.holiash.longestplateau.model;

public class LongestPlateau {

  private int[] array;
  private int arrayLength;
  private int indexOfLongestPlateau;
  private int biggestLength;

  public LongestPlateau(int[] array) {
    if (array == null) {
      throw new NullPointerException();
    }
    this.array = array;
    this.arrayLength = array.length;
    this.biggestLength = 0;
    this.indexOfLongestPlateau = -1;
  }

  public int[] findLongestPlateau() {
    int indexOfCurrentPlateau = 1;
    int currentIndex = 1;
    int currentLength = 1;
    boolean isPlateauStarted = false;
    while (currentIndex < arrayLength) {
      if (startOfPlateau(currentIndex)) {
        isPlateauStarted = true;
        indexOfCurrentPlateau = currentIndex;
      } else if (endOfPlateau(currentIndex)) {
        if (isPlateauStarted && currentLength > biggestLength) {
          indexOfLongestPlateau = indexOfCurrentPlateau;
          biggestLength = currentLength;
        }
        isPlateauStarted = false;
        currentLength = 1;
      } else {
        currentLength++;
      }
      currentIndex++;
    }
    return new int[]{biggestLength, indexOfLongestPlateau};
  }

  private boolean endOfPlateau(int index) {
    if (index <= 0) {
      return false;
    } else {
      return array[index - 1] > array[index];
    }
  }

  private boolean startOfPlateau(int index) {
    if (index <= 0) {
      return false;
    } else {
      return array[index - 1] < array[index];
    }
  }
}
