package com.holiash.longestplateau.view;

import static com.holiash.util.ConsoleLogger.*;

public class PlateauViewImpl implements PlateauView {

  @Override
  public void show(int[] arr) {
    LOGGER.info("Length of longest plateau: " + arr[0]);
    LOGGER.info("Position of longest plateau: " + arr[1]);
  }
}
