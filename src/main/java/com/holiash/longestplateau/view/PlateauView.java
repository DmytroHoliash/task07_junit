package com.holiash.longestplateau.view;

public interface PlateauView {
  void show(int[] arr);
}
