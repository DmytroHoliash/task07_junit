package com.holiash;

import com.holiash.longestplateau.controller.LongestPlateauController;
import com.holiash.longestplateau.view.PlateauViewImpl;
import com.holiash.minesweeper.Minesweeper;
import com.holiash.util.Command;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class Menu {

  private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
  private Map<Integer, Command> menu;
  private Random rand;

  public Menu() {
    rand = new Random();
    menu = new LinkedHashMap<>();
    menu.put(1, this::plateau);
    menu.put(2, this::mineSweeper);
  }

  private void plateau() {
    System.out.println("Input length of array: ");
    try {
      int n = Integer.parseInt(br.readLine());
      while (n <= 0) {
        System.out.println("Input correct value: ");
        n = Integer.parseInt(br.readLine());
      }
      int[] array = new int[n];
      for (int i = 0; i < n; i++) {
        array[i] = rand.nextInt(6);
      }
      LongestPlateauController controller = new LongestPlateauController(array,
        new PlateauViewImpl());
      controller.execute();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void mineSweeper() {
    System.out.println("Input M, N, probability(0, 1): ");
    try {
      int M = Integer.parseInt(br.readLine());
      int N = Integer.parseInt(br.readLine());
      double prob = Double.parseDouble(br.readLine());
      Minesweeper minesweeper = new Minesweeper(M, N, prob);
      minesweeper.initField();
      minesweeper.showField();
      minesweeper.solve();
      minesweeper.showField();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void execute(int key) {
    if (menu.containsKey(key)) {
      this.menu.get(key).execute();
    } else {
      System.out.println("Wrong choice!!!");
    }
  }

  public void print() {
    System.out.println("1 - plateau");
    System.out.println("2 - minesweeper");
    System.out.println("9 - exit");
  }
}
