package com.holiash.util;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleLogger {
  public static final Logger LOGGER = LogManager.getLogger(ConsoleLogger.class);
}
