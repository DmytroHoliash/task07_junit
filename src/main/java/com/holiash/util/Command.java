package com.holiash.util;

public interface Command {
  void execute();
}
