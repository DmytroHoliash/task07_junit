package com.holiash.testing;

public interface UnaryOp {
  int increment();
  int decrement();
}
