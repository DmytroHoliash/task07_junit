package com.holiash.testing;

public class Tested {

  private UnaryOp op;
  private int res;

  public Tested(UnaryOp op) {
    this.op = op;
    res = 0;
  }

  public void setRes(int res) {
    this.res = res;
  }


  public int getRes() {
    return res;
  }

  public int doAddition(int i) {
    for (int j = 0; j < i; j++) {
      res += op.increment();
    }
    return res;
  }

  public void doSubtraction(int i) {
    for (int j = 0; j < i; j++) {
      res += op.decrement();
    }
  }

  public int someMethod(int i) {
    if (i == 0) {
      throw new IllegalArgumentException();
    }
    return i;
  }

  public int sumArr(int[] arr) {
    int sum = 0;
    for (int i = 0; i < arr.length; i++) {
      sum += arr[i];
    }
    return sum;
  }
}
