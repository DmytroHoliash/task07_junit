package com.holiash.minesweeper;

public class Minesweeper {

  private char[][] field;
  private double probability;
  private int columns;
  private int rows;

  public Minesweeper(int M, int N, double probability) {
    if (M <= 1 || N <= 1 || probability < 0. || probability > 1.) {
      throw new IllegalArgumentException("Wrong value");
    }
    this.probability = probability;
    this.field = new char[M][N];
    this.columns = N;
    this.rows = M;
  }

  public char[][] initField() {
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        field[i][j] = (Math.random() < probability) ? '*' : '-';
      }
    }
    return this.field;
  }

  public void showField() {
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        System.out.print(field[i][j]);
      }
      System.out.println();
    }
  }

  public char[][] solve() {
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        if (field[i][j] == '-') {
          field[i][j] = countBombs(i, j);
        }
      }
    }
    return this.field;
  }

  private char countBombs(int i, int j) {
    char counter = '0';
    if ((j - 1) != -1) {
      if (field[i][j - 1] == '*') {
        counter++;
      }
    }
    if ((i - 1) != -1) {
      if (field[i - 1][j] == '*') {
        counter++;
      }
    }
    if ((i - 1) != -1 && (j - 1) != -1) {
      if (field[i - 1][j - 1] == '*') {
        counter++;
      }
    }
    if ((i - 1) != -1 && (j + 1) != columns) {
      if (field[i - 1][j + 1] == '*') {
        counter++;
      }
    }
    if ((i + 1) != rows) {
      if (field[i + 1][j] == '*') {
        counter += 1;
      }
    }
    if ((j + 1) != columns) {
      if (field[i][j + 1] == '*') {
        counter++;
      }
    }
    if ((i + 1) != rows && (j + 1) != columns) {
      if (field[i + 1][j + 1] == '*') {
        counter++;
      }
    }
    if ((i + 1) != rows && (j - 1) != -1) {
      if (field[i + 1][j - 1] == '*') {
        counter++;
      }
    }
    return counter;
  }
}
