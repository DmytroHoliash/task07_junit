package com.holiash;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.holiash.testing.Tested;
import com.holiash.testing.UnaryOp;
import java.util.stream.Stream;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
public class TestedTest {

  @InjectMocks
  Tested tested;

  @Mock
  UnaryOp op;

  @BeforeEach
  public void before() {
    tested.setRes(0);
  }

  @Test
  public void testDoAddition() {
    when(op.increment()).thenReturn(1);
    int res = tested.doAddition(5);
  }

  @Test
  public void testDoSubtraction() {
    when(op.decrement()).thenReturn(-1);
    tested.doSubtraction(5);
    assertEquals(-5, tested.getRes());
    verify(op, times(5)).decrement();
  }

  @Test
  public void testSomeMethod1() {
    assertThrows(IllegalArgumentException.class, () -> tested.someMethod(0));
  }

  @RepeatedTest(value = 2)
  public void testSomeMethod2() {
    assertEquals(1, tested.someMethod(1));
  }

  @ParameterizedTest
  @MethodSource("arrayStream")
  public void testSumArr(int[] arr) {
    assertEquals(tested.sumArr(arr), 25);
  }

  static Stream<int[]> arrayStream() {
    return Stream.of(new int[]{10, 15}, new int[]{5, 20});
  }
}
