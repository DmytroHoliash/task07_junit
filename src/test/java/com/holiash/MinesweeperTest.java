package com.holiash;

import static org.junit.jupiter.api.Assertions.*;

import com.holiash.minesweeper.Minesweeper;
import org.junit.jupiter.api.Test;

public class MinesweeperTest {


  @Test
  public void testInitField1() {
    Minesweeper minesweeper = new Minesweeper(2, 2, 1);
    char[][] res = minesweeper.initField();
    assertArrayEquals(res, new char[][]{{'*', '*'}, {'*', '*'}});
  }

  @Test
  public void testInitField2() {
    Minesweeper minesweeper = new Minesweeper(2, 2, 0);
    char[][] res = minesweeper.initField();
    assertArrayEquals(res, new char[][]{{'-', '-'}, {'-', '-'}});
  }

  @Test
  public void testSolve1() {
    Minesweeper minesweeper = new Minesweeper(2, 2, 1);
    minesweeper.initField();
    char[][] res = minesweeper.solve();
    assertArrayEquals(res, new char[][]{{'*', '*'}, {'*', '*'}});
  }

  @Test
  public void testSolve2() {
    Minesweeper minesweeper = new Minesweeper(2, 2, 0);
    minesweeper.initField();
    char[][] res = minesweeper.solve();
    assertArrayEquals(res, new char[][]{{'0', '0'}, {'0', '0'}});
  }
}
