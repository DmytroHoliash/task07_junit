package com.holiash;

import static org.junit.jupiter.api.Assertions.*;

import com.holiash.longestplateau.model.LongestPlateau;
import org.junit.jupiter.api.*;

public class LongestPlateauTest {

  @Test
  public void testFindLongestPlateau1() {
    LongestPlateau lp = new LongestPlateau(new int[]{1, 5, 5, 5, 3});
    int[] res = lp.findLongestPlateau();
    assertArrayEquals(res, new int[]{3, 1});
  }

  @Test
  public void testFindLongestPlateau2() {
    LongestPlateau lp = new LongestPlateau(new int[]{1, 5, 5, 5, 7});
    int[] res = lp.findLongestPlateau();
    assertArrayEquals(res, new int[]{0, -1});
  }
}
